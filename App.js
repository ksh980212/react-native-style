import React, {Component} from 'react';
import {Button, StyleSheet, View} from 'react-native';
import getStyleSheet from './styles';

export default class App extends Component {

  state = {
    darkTheme : false
  }


  toggleTheme = () => {
    this.setState({darkTheme: !this.state.darkTheme});
  }

  render() {

    const {darkTheme} = this.state;

    const styles = getStyleSheet(darkTheme);
    const backgroundColor = StyleSheet.flatten(styles.container).backgroundColor;

    return(
      <View style = {styles.container}>
        <Button title = {backgroundColor} onPress = {this.toggleTheme} />
      </View>
    );
  }

}